<?php
/**
 * Matrix Fixture
 */
class MatrixFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'matrix';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'x_axis' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'y_axis' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'z_axis' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'value' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'x_axis' => 1,
			'y_axis' => 1,
			'z_axis' => 1,
			'value' => 1
		),
	);

}
