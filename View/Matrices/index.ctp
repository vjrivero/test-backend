<?php echo $this->Form->create('Matrices'); ?>
<div class="matrices index">
	<h2><?php echo __('Matrices'); ?></h2>
	
	<div class="">
        <fieldset class="col-lg-6 col-lg-offset-1">

            <div class="form-group">
                <?php echo $this->Form->input("matrix_data" ,array('label' => __('Enter queries'),'placeholder'=>__('Enter queries'),'class'=>"form-control", 'autofocus', 'type' => 'textarea'))?>
            </div>

            <div class="form-group">
                <?php echo $this->Form->input("output_data" ,array('label' => __('Output result'),'class'=>"form-control", 'type' => 'textarea', 'disabled' => 'disabled'))?>
            </div>
         
        </fieldset>

	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Form->submit(__('Execute'), array('class'=>'','type'=>'submit'));?></li>
	</ul>
</div>
<?php echo $this->Form->end(); ?>
<style type="text/css">
	form div {
		clear: none;
		padding: 0px;
		margin-bottom: 0px;
	}
</style>
<script>
$(function() {
  $("textarea").linedtextarea();
});
</script>