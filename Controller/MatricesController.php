<?php
App::uses('AppController', 'Controller');
/**
 * Matrices Controller
 *
 * @property Matrix $Matrix
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class MatricesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Flash', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		if ($this->request->is('post')) {

			if (!empty($this->request->data['Matrices']['matrix_data'])) {
				$query = $this->request->data['Matrices']['matrix_data'];
				$query_arr = explode(PHP_EOL, $query);
				$this->log($query_arr, LOG_DEBUG);
				
				$qty = $query_arr[0];
				$matrix_id = 1;
				$result = array();
				for ($i=0; $i < $qty; $i++) { 
					$info_matrix = explode(' ', $query_arr[$matrix_id]);
					if ($this->__createMatix($info_matrix[0]) && is_numeric($info_matrix[1])) {
						$resp = $this->__processingMatrix(array_slice($query_arr, $matrix_id+1, $info_matrix[1], true), $info_matrix[0]);
						if (!isset($resp['error'])) {
							$result = array_merge($result, $resp['responses']);
						}else{
							$this->Flash->error($resp['error']);
							break;	
						}
					}else{
						$matrix_id++;
						$this->Flash->error(__("Error at line $matrix_id. Must be format 'N M', with 1 <= N <= 100 and 1 <= M <= 1000."));
						break;
					}

					$matrix_id += $info_matrix[1] +1; 
				}
				$this->log($result, LOG_DEBUG);
				$this->request->data['Matrices']['output_data'] = implode(PHP_EOL, $result);
			}else{
				$this->Flash->error(__('Please enter a query.'));
			}
		}
	}

/**
 * Initailize a matrix into DB 
 *
 * @return true if success, false if error
 */	
	private function __createMatix($size = 1){
		if (is_numeric($size)) {
			$this->Matrix->deleteAll(array('Matrix.name' => 'matrix'), false);
			$matrices_list = array();
			for ($i=1; $i <= $size; $i++) { 
				for ($j=1; $j <= $size; $j++) { 
					for ($k=1; $k <= $size; $k++) { 
						$matrix_model['Matrix']['x_axis'] = $i;
						$matrix_model['Matrix']['y_axis'] = $j;
						$matrix_model['Matrix']['z_axis'] = $k;
						array_push($matrices_list, $matrix_model);
					}
				}
			}

			if ($this->Matrix->saveAll($matrices_list)) {
                return true;
            }   
		}
	
		return false;
	}

/**
 * Execute queries over matrix 
 *
 * @return an array with query results
 */	
	private function __processingMatrix($queries = array(), $size = 1){

		$resp = array('responses' => array());

		foreach ($queries as $key => $value) {
			$query = explode(' ', $value);

			if ($query[0] == 'UPDATE') {
				if(count($query) == 5 && is_numeric($query[1]) && is_numeric($query[2]) && is_numeric($query[3]) && is_numeric($query[4])){
					if ( $query[1] >= 1 && $query[1] <= $size 
					  && $query[2] >= 1 && $query[2] <= $size 
					  && $query[3] >= 1 && $query[3] <= $size) {
						if ($query[4] >= -10**9 && $query[4] <= 10**9) {
							$item = $this->Matrix->find('first', array('conditions' => array('x_axis' => $query[1],
																							 'y_axis' => $query[2],
																							 'z_axis' => $query[3])
																		));
							$item['Matrix']['value'] = $query[4];
							$item = $this->Matrix->save($item);
						}else{
							$key++;
							$resp['error'] = "Error at line $key. Constraint violation -10exp9 <= W <= 10exp9.";
							break;		
						}
					}else{
						$key++;
						$resp['error'] = "Error at line $key. Constraint violation 1 <= x,y,z <= $size.";
						break;	
					}

				}else{
					$key++;
					$resp['error'] = "Error at line $key. Line must be format 'UPDATE x y z W' with 1 <= x,y,z <= $size.";
					break;	
				}
			}elseif ($query[0] == 'QUERY') {
				if (count($query) == 7 && is_numeric($query[1]) && is_numeric($query[2]) && is_numeric($query[3]) 
									   && is_numeric($query[4]) && is_numeric($query[5]) && is_numeric($query[6])){
					if ( $query[1] >= 1 && $query[1] <= $size 
					  && $query[2] >= 1 && $query[2] <= $size 
					  && $query[3] >= 1 && $query[3] <= $size 
					  && $query[4] >= 1 && $query[4] <= $size 
					  && $query[5] >= 1 && $query[5] <= $size 
					  && $query[6] >= 1 && $query[6] <= $size ) {
						$x_axis = array('x_axis BETWEEN ? AND ?' => array($query[1] , $query[4]));
						$y_axis = array('y_axis BETWEEN ? AND ?' => array($query[2] , $query[5]));
						$z_axis = array('z_axis BETWEEN ? AND ?' => array($query[3] , $query[6]));
						$item = $this->Matrix->find('first', array('conditions' => array($x_axis, $y_axis, $z_axis), 
																   'fields' => array('sum(Matrix.value) AS stotal')));
						array_push($resp['responses'] , $item[0]['stotal']);
					
					}else{
						$key++;
						$resp['error'] = "Error at line $key. Constraint violation 1 <= x1 <= x2 <= $size or 1 <= y1 <= y2 <= $size or 1 <= z1 <= z2 <= $size.";
						break;	
					}
				}else{
					$key++;
					$resp['error'] = "Error at line $key. Line must be format 'QUERY x1 y1 z1 x2 y2 z2'.";
					break;	
				}
			}else{
				$key++;
				$resp['error'] = "Error at line $key. Line must begin with 'UPDATE' or 'QUERY'";
				break;
			}
		}
	
		return $resp;
	}
}
