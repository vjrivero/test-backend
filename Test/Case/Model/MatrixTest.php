<?php
App::uses('Matrix', 'Model');

/**
 * Matrix Test Case
 */
class MatrixTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.matrix'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Matrix = ClassRegistry::init('Matrix');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Matrix);

		parent::tearDown();
	}

}
